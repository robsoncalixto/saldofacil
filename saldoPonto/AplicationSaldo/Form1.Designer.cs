﻿namespace AplicationSaldo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.selecioneArq = new System.Windows.Forms.Button();
            this.txtCaminho = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnExecutar = new System.Windows.Forms.Button();
            this.dtGridFuncionarios = new System.Windows.Forms.DataGridView();
            this.nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoExtra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoAtraso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoAtual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCriarArq = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridFuncionarios)).BeginInit();
            this.SuspendLayout();
            // 
            // selecioneArq
            // 
            this.selecioneArq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selecioneArq.Location = new System.Drawing.Point(447, 12);
            this.selecioneArq.Name = "selecioneArq";
            this.selecioneArq.Size = new System.Drawing.Size(133, 23);
            this.selecioneArq.TabIndex = 0;
            this.selecioneArq.Text = "Selecionar arquivo";
            this.selecioneArq.UseVisualStyleBackColor = true;
            this.selecioneArq.Click += new System.EventHandler(this.selecioneArq_Click);
            // 
            // txtCaminho
            // 
            this.txtCaminho.Enabled = false;
            this.txtCaminho.Location = new System.Drawing.Point(19, 14);
            this.txtCaminho.Name = "txtCaminho";
            this.txtCaminho.Size = new System.Drawing.Size(422, 20);
            this.txtCaminho.TabIndex = 1;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Title = "Localizar Arquivos";
            // 
            // btnExecutar
            // 
            this.btnExecutar.Location = new System.Drawing.Point(46, 313);
            this.btnExecutar.Name = "btnExecutar";
            this.btnExecutar.Size = new System.Drawing.Size(228, 37);
            this.btnExecutar.TabIndex = 4;
            this.btnExecutar.Text = "Executar ";
            this.btnExecutar.UseVisualStyleBackColor = true;
            this.btnExecutar.Click += new System.EventHandler(this.btnExecutar_Click);
            // 
            // dtGridFuncionarios
            // 
            this.dtGridFuncionarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridFuncionarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nome,
            this.saldoExtra,
            this.saldoAtraso,
            this.saldoAtual});
            this.dtGridFuncionarios.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dtGridFuncionarios.Location = new System.Drawing.Point(19, 41);
            this.dtGridFuncionarios.Name = "dtGridFuncionarios";
            this.dtGridFuncionarios.Size = new System.Drawing.Size(561, 244);
            this.dtGridFuncionarios.TabIndex = 5;
            // 
            // nome
            // 
            this.nome.DataPropertyName = "nome";
            this.nome.HeaderText = "NOME FUNCIONARIO";
            this.nome.Name = "nome";
            this.nome.Width = 181;
            // 
            // saldoExtra
            // 
            this.saldoExtra.DataPropertyName = "saldoExtra";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.saldoExtra.DefaultCellStyle = dataGridViewCellStyle1;
            this.saldoExtra.HeaderText = "SALDO EXTRA";
            this.saldoExtra.Name = "saldoExtra";
            this.saldoExtra.Width = 120;
            // 
            // saldoAtraso
            // 
            this.saldoAtraso.DataPropertyName = "saldoAtraso";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.saldoAtraso.DefaultCellStyle = dataGridViewCellStyle2;
            this.saldoAtraso.HeaderText = "SALDO ATRASO";
            this.saldoAtraso.Name = "saldoAtraso";
            this.saldoAtraso.Width = 120;
            // 
            // saldoAtual
            // 
            this.saldoAtual.DataPropertyName = "saldoAtual";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.saldoAtual.DefaultCellStyle = dataGridViewCellStyle3;
            this.saldoAtual.HeaderText = "SALDO ATUAL";
            this.saldoAtual.Name = "saldoAtual";
            this.saldoAtual.Width = 120;
            // 
            // btnCriarArq
            // 
            this.btnCriarArq.Location = new System.Drawing.Point(327, 313);
            this.btnCriarArq.Name = "btnCriarArq";
            this.btnCriarArq.Size = new System.Drawing.Size(228, 37);
            this.btnCriarArq.TabIndex = 6;
            this.btnCriarArq.Text = "Exportar Excel >>>";
            this.btnCriarArq.UseVisualStyleBackColor = true;
            this.btnCriarArq.Click += new System.EventHandler(this.btnCriarArq_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(108, 292);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(363, 15);
            this.progressBar1.TabIndex = 7;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 359);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnCriarArq);
            this.Controls.Add(this.dtGridFuncionarios);
            this.Controls.Add(this.btnExecutar);
            this.Controls.Add(this.txtCaminho);
            this.Controls.Add(this.selecioneArq);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Saldo Fácil";
            ((System.ComponentModel.ISupportInitialize)(this.dtGridFuncionarios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selecioneArq;
        private System.Windows.Forms.TextBox txtCaminho;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnExecutar;
        private System.Windows.Forms.DataGridView dtGridFuncionarios;
        private System.Windows.Forms.DataGridViewTextBoxColumn nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoExtra;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoAtraso;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoAtual;
        private System.Windows.Forms.Button btnCriarArq;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

