﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicationSaldo.Model
{
  public class Funcionario
    {
        public string nome { get; set; }
        public string saldoExtra { get; set; }
        public string saldoAtraso { get; set; }
        public TimeSpan saldoAtual { get; set; }

        public Funcionario()
        {

        }
        public Funcionario(string _nome,string _saldoExtra,string _saldoAtraso, TimeSpan _saldoAtual)
        {
            nome = _nome;
            saldoExtra = _saldoExtra;
            saldoAtraso = _saldoAtraso;
            saldoAtual = _saldoAtual;

        }
        public Funcionario(Funcionario func)
        {
            nome = func.nome;
            saldoAtual = func.saldoAtual;
            saldoExtra = func.saldoExtra;
            saldoAtraso = func.saldoAtraso;
        }
    }
}
