﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security;
using System.IO;
using AplicationSaldo.Model;

namespace AplicationSaldo
{
    public partial class Form1 : Form
    {
        List<Funcionario> lFunc = new List<Funcionario>();

        public Form1()
        {
            InitializeComponent();
            progressBar1.Visible = false;
            btnCriarArq.Enabled = false;

        }

        private void selecioneArq_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtCaminho.Text = openFileDialog.FileName;
                }
            }
            catch (SecurityException ex)
            {
                MessageBox.Show("Erro de segurança Contate o Administrador da rede.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Detalhes (enviar no email): \n\n" + ex.StackTrace + " \n\n "
                                + " Para obter ajuda mande um email para :\n "
                                + " robsoncaliixto@gmail.com \n",
                                 "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao tentar abrir o arquivo.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Para obter ajuda mande um email para :\n"
                                + " robsoncaliixto@gmail.com \n",
                                 "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error );
            }

        }

        private void btnGerarArq_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog dialogFolder = new FolderBrowserDialog();

            }
            catch (SecurityException ex)
            {
                MessageBox.Show("Erro de segurança Contate o Administrador da rede.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Detalhes (enviar no email): \n\n" + ex.StackTrace + " \n\n "
                                + " Para obter ajuda mande um email para :\n "
                                + " robsoncaliixto@gmail.com \n",
                                 "Erro",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao tentar abrir o arquivo.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Para obter ajuda mande um email para :\n"
                                + " robsoncaliixto@gmail.com \n",
                                 "Erro",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }

        }

        private void btnExecutar_Click(object sender, EventArgs e){
            try{

                if (txtCaminho.Text == "") {
                    MessageBox.Show(" Nenhum arquivo foi selecionado. \n"
                        + "Para contiuar selecione um arquivo e clique em executar",
                           "Erro",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error );
                    return;
                }
                //if (txtGeracaoArq.Text == ""){

                //    var result = MessageBox.Show(" Nenhum caminho para criação do arquivo foi selecionado.\n"
                //                    + " Deseja continuar sem a geração do arquivo ?",
                //                        "Dúvida",
                //                        MessageBoxButtons.YesNo,
                //                        MessageBoxIcon.Question);
                //    if (result == DialogResult.No){
                //        return;
                //    }              
                //}

               
                StreamReader rd = new StreamReader(txtCaminho.Text);
              
                string linha = null;
                string[] linhaSeparadas = null;

                string nome = "";
                string extra = "";
                string atraso;
                TimeSpan atual;

                bool linha1;
                bool linha2;
                bool linha3;

                while ((linha = rd.ReadLine()) != null){

                    linhaSeparadas = linha.Split(';');
                    linha1 = false;
                    linha2 = false;
                    linha3 = false;

                    for (int i = 0; i < linhaSeparadas.Length; i++) {
                        if (linhaSeparadas[i] != ""){
                            if (linhaSeparadas[0] == "Colaborador:")
                                linha1 = true;
                            if (linhaSeparadas[1] == "HORA EXTRA 50%")
                                linha2 = true;
                            if (linhaSeparadas[1] == "ATRASOS")
                                linha3 = true;

                            if (linha1) {
                                nome = linhaSeparadas[3];
                                break;
                            }
                            if (linha2) {
                                extra = linhaSeparadas[12];
                                break;
                            }
                            if (linha3){
                                atraso = linhaSeparadas[12];
                                atual = Convert.ToDateTime(extra) - Convert.ToDateTime(atraso);
                                lFunc.Add(criaFuncionario(nome,extra,atraso,atual));
                                break;
                          }
                        }
                    }
                }
                rd.Close();
                PreencheDataGridView(lFunc);
                btnCriarArq.Enabled = true;
            }
            catch (SecurityException ex)
            {
                MessageBox.Show("Erro de segurança ao tentar abrir o arquivo.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Detalhes (enviar no email): \n\n" + ex.StackTrace + " \n\n "
                                + " Para obter ajuda mande um email para :\n "
                                + " robsoncaliixto@gmail.com \n",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao tentar abrir o arquivo.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Para obter ajuda mande um email para :\n"
                                + " robsoncaliixto@gmail.com \n",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private Funcionario criaFuncionario(string nome, string extra,
                                            string atraso, TimeSpan saldoAtual){

            var func = new Funcionario( nome,
                                        extra,
                                        atraso,
                                        saldoAtual
                                       );
            
            return func;
        }
        
        private void PreencheDataGridView(List<Funcionario> lFuncionario)
        {
            var lNewFunc = lFuncionario.Select(Funcionario => new
            {
                nome = Funcionario.nome,
                saldoExtra = Funcionario.saldoExtra,
                saldoAtraso = Funcionario.saldoAtraso,
                saldoAtual = Funcionario.saldoAtual

            }).ToList();


            dtGridFuncionarios.DataSource = null;
            dtGridFuncionarios.DataSource = lFuncionario;
            dtGridFuncionarios.Refresh();
        }

        private void btnCriarArq_Click(object sender, EventArgs e)
        {

            btnCriarArq.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
            progressBar1.Visible = true;
            progressBar1.MarqueeAnimationSpeed = 1;
            progressBar1.Style = ProgressBarStyle.Marquee;


        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            gerarExcel();

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.MarqueeAnimationSpeed = 0;
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.Value = 100;
            progressBar1.Visible = false;
            btnCriarArq.Enabled = true;
        }
        private void gerarExcel()
        {
            try
            {
                
                Microsoft.Office.Interop.Excel.Application xcelApp = new Microsoft.Office.Interop.Excel.Application();

                if (dtGridFuncionarios.Rows.Count > 0)
                {
                    try
                    {
                        xcelApp.Application.Workbooks.Add(Type.Missing);

                        for (int i = 1; i < dtGridFuncionarios.Columns.Count; i++)
                        {

                            xcelApp.Cells[1, i] = dtGridFuncionarios.Columns[i - 1].HeaderText;
                        }

                        for (int i = 0; i < dtGridFuncionarios.Rows.Count; i++)
                        {

                            for (int j = 0; j < dtGridFuncionarios.Columns.Count; j++)
                            {
                                xcelApp.Cells[i + 2, j + 1] = dtGridFuncionarios.Rows[i].Cells[j].Value.ToString();
                            }
                        }

                        xcelApp.Columns.AutoFit();
                        xcelApp.Visible = true;

                    }
                    catch (Exception ex)
                    {
                        xcelApp.Quit();
                        throw ex;
                    }

                }

            }
            catch (SecurityException ex)
            {
                MessageBox.Show("Erro de segurança ao tentar gerar o excel.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Detalhes (enviar no email): \n\n" + ex.StackTrace + " \n\n "
                                + " Para obter ajuda mande um email para :\n "
                                + " robsoncaliixto@gmail.com \n",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro na geração do excel.\n\n"
                                + "Mensagem : " + ex.Message + " \n\n "
                                + "Para obter ajuda mande um email para :\n"
                                + " robsoncaliixto@gmail.com \n",
                                 "Erro",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
    }
}